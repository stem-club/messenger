import json
import os

from recipients.classroom import ClassroomRecipient
from recipients.discord import DiscordRecipient
from recipients.email import EmailRecipient

with open("settings.default.json") as f:
    settings = json.load(f)

if os.path.exists("settings.json"):
    with open("settings.json") as f:
        settings.update(json.load(f))

new_recipients = []
for recipient in settings["recipients"]:
    if recipient["type"] == "email":
        from_email = recipient["from"]
        spreadsheet = recipient["spreadsheet"]
        spreadsheet_range = recipient["range"]

        cols = recipient["cols"]
        name_col = cols["name"]
        email_col = cols["email"]
        email_opt_col = cols["email_opt_in"]
        filled_form_col = cols["filled_out_form"]

        opt_ins = EmailRecipient.get_email_opt_ins(from_email, spreadsheet, spreadsheet_range, name_col, email_col,
                                                   email_opt_col, filled_form_col)
        new_recipients += opt_ins
    elif recipient["type"] == "classroom":
        from_email = recipient["from"]
        classroom_name = recipient["name"]
        new_recipients.append(ClassroomRecipient(from_email, classroom_name))
    elif recipient["type"] == "discord":
        token = recipient["token"]
        channel_name = recipient["channel"]
        server = recipient["server"]
        new_recipients.append(DiscordRecipient(token, channel_name, server))

settings["recipients"] = new_recipients
