import sys

from gapi import get_api, API
from message import Message
from recipients.recipient import Recipient


class ClassroomRecipient(Recipient):
    def __init__(self, from_email: str, classroom_name: str):
        super(ClassroomRecipient, self).__init__("everyone")
        self.from_email = from_email
        self.course_id = ClassroomRecipient.get_id_by_name(from_email, classroom_name)

    def send_impl(self, message: Message):
        classes = get_api(self.from_email, API.CLASSROOM).courses()
        body = {
            "text": message.content_as_plain_text(),
            "state": "PUBLISHED"
        }
        print("Posted to Classroom")
        classes.announcements().create(courseId=self.course_id, body=body).execute()

    @staticmethod
    def get_id_by_name(from_email: str, classroom_name: str):
        # TODO: Assumes that nobody will have over 100 classes. Generally true, but this code should be updated to
        # search every page.
        results = get_api(from_email, API.CLASSROOM).courses().list(studentId="me", pageSize=100).execute()["courses"]
        for result in results:
            if result["name"] == classroom_name:
                return result["id"]

        print(f"Cannot find classroom with a name of {classroom_name}", file=sys.stderr)
        sys.exit(1)
