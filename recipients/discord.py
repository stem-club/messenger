import sys

import discord

from message import Message
from recipients.recipient import Recipient


class DiscordClient(discord.Client):
    def __init__(self, message: Message, channel_name: str, server_name: str, **options):
        super().__init__(**options)

        self.message = message
        self.channel_name = channel_name
        self.server_name = server_name

    async def on_ready(self):
        print('Logged on as {0}!'.format(self.user))

        for guild in self.guilds:
            if guild.name == self.server_name:
                for channel in guild.channels:
                    if channel.name == self.channel_name:
                        # TODO: Discord requires all messages to be less than 2000 characters. Breaking the message into
                        # lines, then sending each line, will usually work, but isn't an optimal solution. Use a
                        # Markdown parser to find more appropriate breaks.
                        # TODO: For now, I'm going to disable it and hope that nothing's too long
                        # content = self.message.content_as_md()
                        # for line in content.splitlines():
                        #     if line:
                        #         await self.get_channel(channel.id).send(line)
                        await self.get_channel(channel.id).send(self.message.content_as_md())
                break

        await self.logout()


class DiscordRecipient(Recipient):
    def __init__(self, token: str, channel_name: str, server_name: str):
        super(DiscordRecipient, self).__init__("@everyone")
        self.token = token
        self.channel_name = channel_name
        self.server_name = server_name

    def send_impl(self, message: Message):
        discord_client = DiscordClient(message, self.channel_name, self.server_name)
        discord_client.run(self.token)
