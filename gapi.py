import os
import pickle
import sys
from enum import Enum
from typing import Any

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = [
    "https://www.googleapis.com/auth/spreadsheets.readonly",
    "https://www.googleapis.com/auth/gmail.send",
    "https://www.googleapis.com/auth/classroom.announcements",
    "https://www.googleapis.com/auth/classroom.courses.readonly"
]


class API(Enum):
    SHEETS = ("sheets", "v4")
    GMAIL = ("gmail", "v1")
    CLASSROOM = ("classroom", "v1")


def get_creds(gmail: str) -> Any:
    creds = None

    token_path = f"tokens/{gmail}.pickle"
    cred_path = f"credentials/google.json"

    if os.path.exists(token_path):
        with open(token_path, "rb") as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        elif os.path.exists(cred_path):
            print(f"Sign in with account {gmail}")
            flow = InstalledAppFlow.from_client_secrets_file(cred_path, SCOPES)
            creds = flow.run_local_server(port=0)
        else:
            print(f"Invalid gmail \"{gmail}\"", file=sys.stderr)
            sys.exit(1)
        # Save the credentials for the next run
        with open(token_path, "wb") as token:
            pickle.dump(creds, token)
    return creds


def get_api(gmail: str, api: API) -> Any:
    return build(api.value[0], api.value[1], credentials=get_creds(gmail))
