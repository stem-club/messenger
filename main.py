import os

from message import Message
from settings import settings


def get_message() -> Message:
    while True:
        title = ""
        while not title:
            print("What should the message be titled?")
            print("The title is used, eg, as the email subject, and may not always be shown")
            title = input("> ")

        path_to_message = ""
        while not path_to_message and not os.path.exists(path_to_message) and not os.path.isfile(path_to_message):
            print("Enter the path to a markdown file containing the message")
            path_to_message = input("> ")

        with open(path_to_message) as f:
            message = Message(title, f.read())

        print("Confirm message?")
        print("")
        print(message.title.format(name="Alice"))
        print(" ----")
        print(message.content_as_md().format(name="Alice"))
        confirm = input("y/N > ")
        if len(confirm) > 0 and confirm.lower()[0] == "y":
            break

    return message


def message_preferred():
    message = get_message()

    for recipient in settings["recipients"]:
        recipient.send(message)


def run_menu() -> bool:
    print("Menu:")
    print(" 1. Message all of STEM Club via preferred communications")
    # print(" 2. Send an email to all STEM Club members (who are not explicitly opted out)")
    print(" 0. Quit")
    answer = input("> ")

    if answer == "0":
        # Is valid input, but don't have anything to do, so just pass
        pass
    elif answer == "1":
        message_preferred()
    else:
        return False
    return True


if __name__ == "__main__":
    print("STEM Club Messenger")
    print("")

    while not run_menu():
        pass
