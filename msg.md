Hey {name}, don't forget about the STEM Club meeting tomorrow (10/14) at lunch!

PicoCTF (https://picoctf.com) has ended with our A team coming in 11th place. We will be covering PicoCTF
challenges in room 156A (back of the cafeteria). Even if you didn't sign up for Pico but you want to learn about
cybersecurity, feel free to come in. We also encourage you to bring in your laptops if you can.
