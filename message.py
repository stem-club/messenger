import markdown2
from html_text import html_text


class Message:
    def __init__(self, title: str, md_content: str):
        self.title = title
        self.md_content = md_content

    def content_as_md(self) -> str:
        return self.md_content

    def content_as_html(self) -> str:
        return markdown2.markdown(self.md_content)

    def content_as_plain_text(self) -> str:
        return html_text.extract_text(self.content_as_html())
