# README
This project is designed to send messages to many people at once through different channels. At present, it supports
email, Google Classroom, and Discord.

## Enabling Google Integration
Google accounts are used to send Gmail messages and Google Classroom announcements. You can have as many Google accounts
as you want tied to the messenger. My use case was to have one personal Google account that handled all email, and
another school Google account used to send announcements on Google Classroom.

These steps should only be performed once. Each Google account can reuse the same project.

### Step 1: Creating a project
You should perform this step using a Google account that's not in a GSuite to ensure that Google APIs are enabled for
your account. Visit the [Google developer dashboard](https://console.developers.google.com), agree to the Terms of
Service if applicable, and click the project selection menu button in the top left (to the right of the Google APIs
logo). If you haven't created any projects before, the button will say "Select a project". Otherwise, it will have the
name of the last project you modified.

![Image of the project selection menu button](img/ProjectSelectionMenu.png)

Select the "New Project" button in the top right of the popup box. This will take you to a new page where you can
configure the project. Choose a descriptive name; it will show up when you post announcements to Classroom. It can be
changed later.

![Image of the new project configuration page](img/NewProjectConfig.png)

### Step 2: Adding APIs to the project
Check that the project you just created is selected in the project selection menu. Then, click "Library" in the left
sidebar.

![Image of the Library button](img/LeftSidebarLibrary.png)

Within the API Library, search for "Sheets", and click on "Google Sheets API".

![Image of the "Sheets" search results](img/SheetsSearch.png)

Then, choose to Enable the API.

![Image of the "Enable" button for the Sheets API](img/EnableSheets.png)

Wait for the API to be enabled. Disregard the warning about needing credentials; I'll go over that in the next step.
Finally, search for and enable the Gmail and Google Classroom APIs.

![Image of enabling the Gmail API](img/EnableGmail.png)

![Image of enabling the Google Classroom API](img/EnableClassroom.png)

### Step 3: Setting the app name
Click the Google APIs logo at the top left of the screen to return to the home page. Then, select the "OAuth consent
screen" button in the left sidebar.

![Image of the OAuth consent screen button](img/LeftSidebarConsent.png) 

Enter a name into the "Application name" field. I recommend using the same name you used when creating the project. If
you wish, you can add a logo, homepage, privacy policy, and terms of service, but if this software is used as intended,
you will be the only person to ever see these customizations. Only the name is needed.

![Image of the Application name field](img/SetName.png)

At the very bottom of the page, click the blue "Save" button to save your changes.

![Image of the Save button](img/NameSave.png)

### Step 4: Adding credentials
If you're not already, go to the "Credentials" tab by choosing it on on the left sidebar.

Click the "Create credentials" dropdown, then select "OAuth client ID".

![Image of the OAuth client ID option](img/CreateOAuth.png)

Select the "Other" application type and name it "Python app" or any arbitrary name of your choosing. Then, click
"Create".

![Image of setting the application type](img/SetType.png)

Dismiss the dialog box with your client ID and secret.

![Image of dialog box](img/OAuthDialog.png)

Click the JSON download button to the right of the newly created client ID.

![Image of JSON download button](img/DownloadJSON.png)

Create a `credentials` folder in the top level directory of this project (next to `README.md` and
`settings.default.json`) if it does not exist already. Save the file in that folder as as `google.json`.

### Step 5: All ready
You're free to start using email and Classroom recipients in `settings.json`. The first time a new Google account is
used, you will be prompted to log in to that account and link your project to it.

## Enabling Discord Integration
A Discord bot is used to send messages to any number of Discord channels in Discord servers.

### Step 1: Creating an application
Make sure you have a [Discord](https://discordapp.com) account first. Visit the
[Developer Portal](https://discordapp.com/developers/applications), click the "New Application" button in the upper
right, then name your application. When you're done, click "Create".

![Image of image creation dialog](img/CreateApplication.png)

### Step 2 (Optional): Customize the application
On the page you load into after creating a new application, you can optionally select a new icon and write a
description for the app.

### Step 3: Create the bot
Choose the "Bot" tab in the left sidebar, click "Add Bot", and confirm with "Yes, do it!". Customize it if desired.
Uncheck "Public Bot".

![Image of setting up the bot](img/SetUpBot.png)

### Step 4: Connecting the bot
Create a `credentials` folder in the top level directory of this project (next to `README.md` and
`settings.default.json`) if it does not exist already. Within that directory, make and open a file called
`discord.json`. Put the following JSON code into the file:

```json
{
  "token": "BOT-TOKEN"
}
```

Click the "Copy" button under the token header to copy the token. Treat the token like a password and keep it secure.
Paste it into the `discord.json`, replacing the text `BOT-TOKEN`.

### Step 5: Generating the bot add link
Click the "OAuth2" link on the left sidebar. Select the "bot" scope.

![Image of the selected bot scope](img/PickBotScope.png)

In the box that appears below after selecting the scope, select "View Channels" (needed to select which channel to make
announcements to) from the "General Permissions" column and "Send Messages" (needed to make announcements) and "Mention
Everyone" (needed to send notifications to everybody possible) from the "Text Permissions" column.

![Image of choosing the bot's permissions](img/PickPermissions.png)

Copy the link at the bottom of the top box (where you selected "bot").

### Step 6: Adding the bot to your servers
Visit the link you just generated. Choose the server you want to add the bot to, allow all the permissions, and click
"Authorize".

![Image of adding the bot to a server](img/AddToServer.png)

You will need the "Manage Server" permission on the server you want to add the bot to. Repeat this step for every server
you want to add the bot to. You can reuse the same link several times. If you lose the link, you can regenerate it by
following Step 5 again.
