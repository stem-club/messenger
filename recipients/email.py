import base64
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import List

from gapi import get_api, API
from message import Message
from recipients.recipient import Recipient


class EmailRecipient(Recipient):
    def __init__(self, name: str, to_email: str, from_email: str):
        super(EmailRecipient, self).__init__(name)
        self.to_email = to_email
        self.from_email = from_email

    def __str__(self):
        return f"{self.name} ({self.to_email})"

    def send_impl(self, message: Message):
        msg = MIMEMultipart("alternative")
        msg["to"] = self.to_email
        msg["from"] = self.from_email
        msg["subject"] = message.title.format(name=self.name)

        msg.attach(MIMEText(message.content_as_plain_text().format(name=self.name), "plain"))
        msg.attach(MIMEText(message.content_as_html().format(name=self.name), "html"))

        gmail_message = {"raw": base64.urlsafe_b64encode(msg.as_bytes()).decode()}

        service = get_api(self.from_email, API.GMAIL)
        service.users().messages().send(userId="me", body=gmail_message).execute()
        print(f"Sent an email to {self}")

    @staticmethod
    def get_email_opt_ins(from_email: str, spreadsheet_id: str, spreadsheet_range: str, name_col: int,
                          to_email_col: int, email_opt_col: int, filled_form_col: int) -> List["EmailRecipient"]:
        sheet = get_api(from_email, API.SHEETS).spreadsheets()
        result = sheet.values().get(spreadsheetId=spreadsheet_id, range=spreadsheet_range).execute()
        values = result.get("values", [])

        if not values:
            print("Possible error: No email opt ins found")
            return []
        else:
            form_fillers = filter(lambda row: row[filled_form_col] == "TRUE", values)
            email_opt_ins = filter(lambda row: row[email_opt_col] == "TRUE", form_fillers)
            return list(map(lambda row: EmailRecipient(row[name_col], row[to_email_col], from_email), email_opt_ins))
