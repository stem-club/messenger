import abc

from message import Message


class Recipient:
    __metaclass__ = abc.ABCMeta

    def __init__(self, name: str = "everyone"):
        self.name = name

    def send(self, message: Message):
        new_title = message.title.format(name=self.name)
        new_content = message.content_as_md().format(name=self.name)
        message = Message(new_title, new_content)
        self.send_impl(message)

    @abc.abstractmethod
    def send_impl(self, message: Message):
        return
